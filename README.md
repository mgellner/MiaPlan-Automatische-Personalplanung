# MiaPlan - Automatische Personalplanung

Issue Tracker für MiaPlan, dem datenschutzfreundlichen Tool zur Schichtplanung

Hier geht's weiter zu den [Features und Fehlermeldungen](https://codeberg.org/mgellner/MiaPlan-Automatische-Personalplanung/issues)

## Fragen und Diskussionen

Fragen und Diskussionen haben Platz in der deutschen [Lemmy-Community](https://feddit.de/c/miaplan) von MiaPlan.

## Soziale Medien

MiaPlan tootet auf <a rel="me" href="https://social.tchncs.de/@miaplan">Mastodon</a>